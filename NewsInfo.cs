﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess
{
    public class NewsInfo
    {
        private DateTime date;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        private string currency;

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        private string events;

        public string Events
        {
            get { return events; }
            set { events = value; }
        }

        private string importance;

        public string Importance
        {
            get { return importance; }
            set { importance = value; }
        }

        private string actual;

        public string Actual
        {
            get { return actual; }
            set { actual = value; }
        }

        private string forecast;

        public string Forecast
        {
            get { return forecast; }
            set { forecast = value; }
        }

        private string previous;

        public string Previous
        {
            get { return previous; }
            set { previous = value; }
        }

        private int newsID;

        public int NewsID
        {
            get { return newsID; }
            set { newsID = value; }
        }

        private DateTime updatedDate;

        public DateTime UpdatedDate
        {
            get { return updatedDate; }
            set { updatedDate = value; }
        }
    }
}
